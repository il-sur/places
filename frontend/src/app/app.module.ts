import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CategoryService} from "./services/category.service";
import {PlaceService} from "./services/place.service";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { CategoryComponent } from './components/category/category.component';
import { CategoryCardComponent } from './components/category-card/category-card.component';
import { CategoryDetailsComponent } from './components/category-details/category-details.component';
import { CategoryEditModalComponent } from './components/category-edit-modal/category-edit-modal.component';
import { PlaceComponent } from './components/place/place.component';
import { PlaceCardComponent } from './components/place-card/place-card.component';
import { PlaceDetailsComponent } from './components/place-details/place-details.component';
import { PlaceEditModalComponent } from './components/place-edit-modal/place-edit-modal.component';
import { PlaceFindComponent } from './components/place-find/place-find.component';
import { PlaceFindCardComponent } from './components/place-find-card/place-find-card.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    CategoryCardComponent,
    CategoryDetailsComponent,
    CategoryEditModalComponent,
    PlaceComponent,
    PlaceCardComponent,
    PlaceDetailsComponent,
    PlaceEditModalComponent,
    PlaceFindComponent,
    PlaceFindCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    PlaceService,
    CategoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
