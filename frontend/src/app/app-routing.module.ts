import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PlaceComponent} from "./components/place/place.component";
import {PlaceDetailsComponent} from "./components/place-details/place-details.component";
import {CategoryComponent} from "./components/category/category.component";
import {CategoryDetailsComponent} from "./components/category-details/category-details.component";
import {PlaceFindComponent} from "./components/place-find/place-find.component";

const routes: Routes = [
  {path: 'places', component: PlaceComponent},
  {path: 'place/:id', component: PlaceDetailsComponent},
  {path: 'categories', component: CategoryComponent},
  {path: 'category/:id', component: CategoryDetailsComponent},
  {path: 'find', component: PlaceFindComponent},
  {path: '**', redirectTo: '/places'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
