import { Component, OnInit } from '@angular/core';
import {CategoryService} from "../../services/category.service";
import {Category} from "../../domain/category";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  categories: Category[] = [];

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.load();
  }

  onSave() {
    this.load();
  }

  private load() {
    this.categoryService.findAll().subscribe(
      data => this.categories = data,
      error => console.log(error)
    );
  }
}
