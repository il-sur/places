import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Place} from "../../domain/place";
import {Category} from "../../domain/category";

@Component({
  selector: 'app-place-card',
  templateUrl: './place-card.component.html',
  styleUrls: ['./place-card.component.scss']
})
export class PlaceCardComponent implements OnInit {
  @Input() place: Place;
  @Input() category: Category;
  @Output() like = new EventEmitter();
  @Output() dislike = new EventEmitter();
  @Output() edit = new EventEmitter();
  @Output() remove = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onLike() {
    this.like.emit();
  }

  onDislike() {
    this.dislike.emit();
  }

  onRemove() {
    this.remove.emit();
  }

  onEdit() {
    this.edit.emit();
  }


}
