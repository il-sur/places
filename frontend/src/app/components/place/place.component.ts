import { Component, OnInit } from '@angular/core';
import {PlaceService} from "../../services/place.service";
import {Place} from "../../domain/place";

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.scss']
})
export class PlaceComponent implements OnInit {
  places: Place[] = [];

  constructor(private placeService: PlaceService) { }

  ngOnInit() {
    this.load();
  }

  onSave() {
    this.load();
  }

  onLike(id: number) {
    this.placeService.like(id).subscribe(
      _ => this.load(),
      error => console.log(error)
    );
  }

  onDislike(id: number) {
    this.placeService.dislike(id).subscribe(
      _ => this.load(),
      error => console.log(error)
    );
  }

  onRemove(id: number) {
    this.placeService.remove(id).subscribe(
      _ => this.load(),
      error => console.log(error)
    );
  }

  private load() {
    this.placeService.getPlaces().subscribe(
      data => this.places = data,
      error => console.log(error)
    );
  }
}
