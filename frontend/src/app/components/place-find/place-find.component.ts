import { Component, OnInit } from '@angular/core';
import {PlaceService} from "../../services/place.service";
import {Place} from "../../domain/place";

@Component({
  selector: 'app-place-find',
  templateUrl: './place-find.component.html',
  styleUrls: ['./place-find.component.scss']
})
export class PlaceFindComponent implements OnInit {
  places: Place[] = [];
  place: Place;
  constructor(private placeService: PlaceService) { }

  ngOnInit() {
  }

  onSearchByName(name: string) {
    this.placeService.findByName(name).subscribe(
      data => this.places = data,
      error => console.log(error)
    );
  }

}
