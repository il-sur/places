import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from "../../domain/category";

@Component({
  selector: 'app-category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.scss']
})
export class CategoryCardComponent implements OnInit {
  @Input() category: Category;
  @Output() edit = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
