import { Component, OnInit } from '@angular/core';
import {PlaceService} from "../../services/place.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Category} from "../../domain/category";
import {Place} from "../../domain/place";

@Component({
  selector: 'app-place-details',
  templateUrl: './place-details.component.html',
  styleUrls: ['./place-details.component.scss']
})
export class PlaceDetailsComponent implements OnInit {
  place: Place;
  category: Category;
  private id: number;

  constructor(private placeService: PlaceService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    if (isNaN(this.id)) {
      this.router.navigate(['/places']);
      return;
    }
    this.load(this.id);
  }

  onLike(id: number) {
    this.placeService.like(id).subscribe(
      _ => this.load(this.id),
      error => console.log(error)
    );
  }

  onDislike(id: number) {
    this.placeService.dislike(id).subscribe(
      _ => this.load(this.id),
      error => console.log(error)
    );
  }

  onRemove(id: number) {
    this.placeService.remove(id).subscribe(
      _ => this.router.navigate(['/places']),
      error => console.log(error)
    );
  }

  onChange() {
    this.load(this.id);
  }

  private load(id) {
    this.placeService.getById(id).subscribe(
      data => this.place = data,
      error => console.log(error)
    );
  }

}
