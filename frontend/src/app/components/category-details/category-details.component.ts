import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PlaceService} from "../../services/place.service";
import {CategoryService} from "../../services/category.service";
import {Place} from "../../domain/place";
import {Category} from "../../domain/category";

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.scss']
})
export class CategoryDetailsComponent implements OnInit {
  category: Category;
  places: Place[] = [];
  place: Place;

  constructor(private categoryService: CategoryService,
              private placeService: PlaceService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    if (isNaN(id)) {
      this.router.navigate(['/categories']);
      return;
    }
    this.categoryService.findById(id).subscribe(
      data => this.category = data,
      error => console.log(error)
    );
    this.onLoad();
  }

  private onLoad() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.placeService.getAllByCategoryId(id).subscribe(
      data => this.places = data,
      error => console.log(error)
    );
  }

}
