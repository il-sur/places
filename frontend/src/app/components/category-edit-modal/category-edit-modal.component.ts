import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Category} from "../../domain/category";
import {CategoryService} from "../../services/category.service";
import {NgForm} from "@angular/forms";
declare const $: any;

@Component({
  selector: 'app-category-edit-modal',
  templateUrl: './category-edit-modal.component.html',
  styleUrls: ['./category-edit-modal.component.scss']
})
export class CategoryEditModalComponent implements OnInit {
  @Output() save = new EventEmitter();
  @ViewChild('categoryEditForm') form: NgForm;
  category: Category;

  constructor(private categoryService: CategoryService) {
    this.clear();
  }

  ngOnInit() {
    $(document).ready(function(){
      $('#categoryEditModal').modal({
        dismissible: false
      });
    });
  }

  onAdd(){
    this.clear();
    this.show();
  }

  // onEdit(id: number){
  //   this.categoryService.findById(id).subscribe(
  //     data =>{
  //       this.category = data;
  //       this.show();
  //     },
  //     error => console.log(error)
  //   );
  // }

  onSave(){
    if(this.category.id === 0){
      this.categoryService.addCategory(this.category).subscribe(
        _ => {
          this.save.emit();
          this.clear();
          this.close();
          this.reset();
        },
        error => console.log(error)
      )
    }
  }

  onCancel(){
    this.clear();
    this.close();
    this.reset();
  }

  private clear(){
    this.category = new Category(0, '');
  }

  private close(){
    $('#categoryEditModal').modal('close');
  }

  private reset(){
    this.form.reset();
  }

  private show() {
    $('#categoryEditModal').modal('open');
  }


}
