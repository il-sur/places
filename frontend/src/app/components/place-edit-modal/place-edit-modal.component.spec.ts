import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceEditModalComponent } from './place-edit-modal.component';

describe('PlaceEditModalComponent', () => {
  let component: PlaceEditModalComponent;
  let fixture: ComponentFixture<PlaceEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceEditModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
