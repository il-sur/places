import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Place} from "../../domain/place";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";
import {PlaceService} from "../../services/place.service";
import {Category} from "../../domain/category";
import {NgForm} from "@angular/forms";
declare const $: any;

@Component({
  selector: 'app-place-edit-modal',
  templateUrl: './place-edit-modal.component.html',
  styleUrls: ['./place-edit-modal.component.scss']
})
export class PlaceEditModalComponent implements OnInit {
  @Output() save = new EventEmitter();
  @ViewChild('placeEditForm') form: NgForm;

  place: Place;
  places: Place[] = [];
  category: Category;
  categories: Category[] = [];
  categoryId: number;

  constructor(private placeService: PlaceService,
              private categoryService: CategoryService,
              private route: ActivatedRoute,
              private router: Router) {
    this.clear();
  }

  ngOnInit() {
    $(document).ready(function() {
      $('#placeEditModal').modal({
        dismissible: false
      });
    });
    this.onLoadCategories();
  }

  onAdd() {
    this.clear();
    this.show();
  }

  onEdit(id: number) {
    this.placeService.getById(id).subscribe(
      data => {
        this.place = data;
        this.show();
      },
      error => console.log(error)
    );

  }

  onLoadCategories() {
    this.categoryService.findAll().subscribe(
      data => this.categories = data,
      error => console.log(error)
    );
  }


  onSave() {
    if (this.place.id === 0) {
      this.placeService.addPlace(this.place, this.categoryId).subscribe(
        _ => {
          this.save.emit();
          this.clear();
          this.close();
          this.reset();
        },
        error => console.log(error)
      );
    } else {
      this.placeService.editPlace(this.place.id, this.place).subscribe(
        _ => {
          this.save.emit();
          this.clear();
          this.close();
          this.reset();
        },
        error => console.log(error)
      );
    }
  }

  onCancel() {
    this.clear();
    this.close();
    this.reset();
  }

  private clear() {
    this.place = new Place(0, '', '', '', '', 0/*, this.category*/);
  }

  private close() {
    $('#placeEditModal').modal('close');
  }

  private reset() {
    this.form.reset();
  }

  private show() {
    $('#placeEditModal').modal('open');
  }

}
