import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceFindCardComponent } from './place-find-card.component';

describe('PlaceFindCardComponent', () => {
  let component: PlaceFindCardComponent;
  let fixture: ComponentFixture<PlaceFindCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceFindCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceFindCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
