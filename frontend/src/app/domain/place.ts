

export class Place {
  constructor(public id:          number,
              public photo:       string,
              public name:        string,
              public address:     string,
              public description: string,
              public rating:      number
  ) {
  }
}
