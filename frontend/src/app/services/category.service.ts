import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Category} from "../domain/category";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) { }

  findAll(): Observable<Category[]> {
    return this.http.get<Category[]>(`${environment.api}/categories`);
  }

  findById(id: number): Observable<Category> {
    return this.http.get<Category>(`${environment.api}/categories/${id}`);
  }

  addCategory(category: Category): Observable<any> {
    return this.http.post(`${environment.api}/categories`,
      category,
      {responseType: 'text',
        observe: 'response'});
  }
}
