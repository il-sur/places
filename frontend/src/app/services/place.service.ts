import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Place} from "../domain/place";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PlaceService {
  constructor(private http: HttpClient) { }

  getPlaces(): Observable<Place[]> {
    return this.http.get<Place[]>(`${environment.api}/places`);
  }

  getById(id: number): Observable<Place> {
    return this.http.get<Place>(`${environment.api}/places/${id}`);
  }

  getAllByCategoryId(category_id: number): Observable<Place[]> {
    return this.http.get<Place[]>(`${environment.api}/places/category/${category_id}`);
  }


  findByName(name: string): Observable<Place[]> {
    return this.http.get<Place[]>(`${environment.api}/places/${name}/searchByName`);
  }

  addPlace(place: Place, id: number): Observable<any> {
    return this.http.post(`${environment.api}/places/category/${id}`,
      place,
      {responseType: 'text',
        observe: 'response'});
  }

  remove(id: number): Observable<any> {
    return this.http.delete(`${environment.api}/places/${id}`,
      {responseType: 'text',
        observe: 'response'});
  }

  editPlace(id: number, place: Place): Observable<any> {
    return this.http.put(`${environment.api}/places/${id}/edit`,
      place,
      {responseType: 'text',
        observe: 'response'});
  }

  like(id: number): Observable<any> {
    return this.http.post(`${environment.api}/places/${id}/rating`,
      null,
      {responseType: 'text',
        observe: 'response'});
  }

  dislike(id: number): Observable<any> {
    return this.http.delete(`${environment.api}/places/${id}/rating`,
      {responseType: 'text',
        observe: 'response'});
  }
}
