package ru.ilsur.backend.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import ru.ilsur.backend.entity.Category;
import ru.ilsur.backend.service.CategoryService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/categories")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    @ApiOperation("Показывает все категории")
    public List<Category> getAll(){
        return categoryService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation("Вызов по id")
    public Category getById(@PathVariable int id){
        return categoryService.getCategoryById(id);
    }

    @PostMapping
    @ApiOperation("Добавление категории")
    public void addCategory(@RequestBody Category category){
        categoryService.addCategory(category);
    }

}
