package ru.ilsur.backend.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import ru.ilsur.backend.entity.Place;
import ru.ilsur.backend.service.PlaceService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/places")
public class PlaceController {
    private final PlaceService placeService;

    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping
    @ApiOperation("Показывает все заведения")
    public List<Place> getAll(){
        return placeService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation("Вызов заведения по id")
    public Place findById(@PathVariable int id){
        return placeService.findById(id);
    }

    @GetMapping("/{name}/searchByName")
    @ApiOperation("Поиск по названию")
    public List<Place> getByName(@PathVariable String name){
        return placeService.getByName(name);
    }

    @PostMapping("/category/{categoryId}")
    @ApiOperation("Добавление по заведения по id категории")
    public void addPlace(@RequestBody @Valid Place place, @PathVariable int categoryId){
        placeService.addPlace(place, categoryId);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление по id")
    public void deletePlace(@PathVariable int id){
        placeService.deletePlace(id);
    }

    @PutMapping("/{id}/edit")
    @ApiOperation("Редактирование")
    public void editPlace(@PathVariable int id, @RequestBody @Valid Place place){
        placeService.editPlace(id, place);
    }

    @PostMapping("/{id}/rating")
    @ApiOperation("Лайк")
    public void like(@PathVariable int id){
        placeService.likeById(id);
    }

    @DeleteMapping("/{id}/rating")
    @ApiOperation("Дизлайк")
    public void dislike(@PathVariable int id){
        placeService.dislikeById(id);
    }

    @GetMapping("/category/{categoryId}")
    @ApiOperation("Показывает заведения определенной категории")
    public List<Place> getPlacesOfCategory(@PathVariable int categoryId){
        return placeService.findAllByCategory(categoryId);
    }
}
