package ru.ilsur.backend.service;

import org.springframework.stereotype.Service;
import ru.ilsur.backend.entity.Place;
import ru.ilsur.backend.exception.CategoryNotFoundException;
import ru.ilsur.backend.exception.PlaceNotFoundException;
import ru.ilsur.backend.repository.CategoryRepository;
import ru.ilsur.backend.repository.PlaceRepository;

import java.util.List;

@Service
public class PlaceServiceImpl implements PlaceService {
    private final CategoryRepository categoryRepository;
    private final PlaceRepository placeRepository;

    public PlaceServiceImpl(CategoryRepository categoryRepository, PlaceRepository placeRepository) {
        this.categoryRepository = categoryRepository;
        this.placeRepository = placeRepository;
    }

    @Override
    public List<Place> getAll(){
        return placeRepository.findAll();
    }

    @Override
    public Place findById(int id){
        return placeRepository.findById(id).orElseThrow(PlaceNotFoundException::new);
    }

    @Override
    public List<Place> getByName(String name){
        return placeRepository.findByNameIgnoreCase(name);
    }

    @Override
    public void addPlace(Place place, int categoryId){
        place.setCategory(categoryRepository.findById(categoryId).orElseThrow(CategoryNotFoundException::new));
        placeRepository.save(place);
    }

    @Override
    public void deletePlace(int id){
        placeRepository.deleteById(id);
    }

    @Override
    public void editPlace(int id, Place place){
        Place edPlace = placeRepository.findById(id).orElseThrow(PlaceNotFoundException::new);
        edPlace.setName(place.getName());
        edPlace.setAddress(place.getAddress());
        edPlace.setPhoto(place.getPhoto());
        edPlace.setDescription(place.getDescription());
        edPlace.setCategory(place.getCategory());
        placeRepository.save(edPlace);
    }

    @Override
    public void likeById(int id){
        placeRepository.likeById(id);
    }

    @Override
    public void dislikeById(int id){
        placeRepository.dislikeById(id);
    }

    @Override
    public List<Place> findAllByCategory(int categoryId) {
        return placeRepository.findPlacesByCategory_Id(categoryId);
    }

}
