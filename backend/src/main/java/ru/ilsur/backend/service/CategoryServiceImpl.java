package ru.ilsur.backend.service;

import org.springframework.stereotype.Service;
import ru.ilsur.backend.entity.Category;
import ru.ilsur.backend.exception.CategoryNotFoundException;
import ru.ilsur.backend.repository.CategoryRepository;
import ru.ilsur.backend.repository.PlaceRepository;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{
    private final CategoryRepository categoryRepository;
    private final PlaceRepository placeRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository, PlaceRepository placeRepository) {
        this.categoryRepository = categoryRepository;
        this.placeRepository = placeRepository;
    }

    @Override
    public List<Category> getAll(){
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategoryById(int id){
        return categoryRepository.findById(id).orElseThrow(CategoryNotFoundException::new);
    }

    @Override
    public void addCategory(Category category) {
        categoryRepository.save(category);
    }

}
