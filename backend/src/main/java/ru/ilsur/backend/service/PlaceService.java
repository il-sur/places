package ru.ilsur.backend.service;

import ru.ilsur.backend.entity.Place;

import java.util.List;

public interface PlaceService {
    List<Place> getAll();

    Place findById(int id);

    List<Place> getByName(String name);

    void addPlace(Place place, int categoryId);

    void deletePlace(int id);

    void editPlace(int id, Place place);

    void likeById(int id);

    void dislikeById(int id);

    List<Place> findAllByCategory(int categoryId);

}
