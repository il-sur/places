package ru.ilsur.backend.service;

import ru.ilsur.backend.entity.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();

    Category getCategoryById(int id);

    void addCategory(Category category);

    //List<Place> findAllPlaces(int id);
}
