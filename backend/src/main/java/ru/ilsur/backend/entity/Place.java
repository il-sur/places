package ru.ilsur.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Place {
    @Id
    @GeneratedValue
    private int id;
    private String photo;
    @Column(nullable = false, unique = true)
    @NotEmpty(message = "Name can't be empty")
    @NotNull
    private String name;
    @Column(nullable = false, unique = true)
    @NotEmpty(message = "Address can't be empty")
    @NotNull
    private String address;
    @Column(columnDefinition = "TEXT")
    @NotEmpty(message = "Description can't be empty")
    @Size(min = 10, max = 500, message = "The description size should be between 10 and 500")
    private String description;

    private int rating;

    @ManyToOne(targetEntity = Category.class)
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private Category category;

}
