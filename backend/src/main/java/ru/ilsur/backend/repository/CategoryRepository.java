package ru.ilsur.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ilsur.backend.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
