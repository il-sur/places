package ru.ilsur.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ilsur.backend.entity.Place;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface PlaceRepository extends JpaRepository<Place, Integer> {

    List<Place> findByNameIgnoreCase(String name);

    List<Place> findPlacesByCategory_Id(int categoryId);

    @Modifying
    @Transactional
    @Query("update Place p set p.rating = p.rating + 1 where p.id = :id"/* and p.category.id = :categoryId"*/)
    void likeById(@Param("id") int id/*, @Param("categoryId") int categoryId*/);

    @Modifying
    @Transactional
    @Query("update Place p set p.rating = p.rating - 1 where p.id = :id"/* and p.category.id = :categoryId"*/)
    void dislikeById(@Param("id") int id/*, @Param("categoryId") int categoryId*/);

}
